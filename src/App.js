import logo from "./logo.svg";
import "./App.css";
import Navbar from "./Ex_buoi1/Navbar";
import Header from "./Ex_buoi1/Header";
import PageContent from "./Ex_buoi1/PageContent";
import Footer from "./Ex_buoi1/Footer";

function App() {
  return (
    <div>
      <Navbar />
      <Header />
      <PageContent />
      <Footer />
    </div>
  );
}

export default App;
